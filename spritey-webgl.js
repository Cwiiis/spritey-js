
/*
 * Copyright (c) 2014, 2015 Chris Lord
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

function SpriteyWebGL(spritey) {
  this.s = spritey;
  this.canvas = spritey.canvas;

  this.matrixStack = { length: 0 };

  this.mvMatrix = mat4.create();
  this.pMatrix = mat4.create();

  this.aTexture = null;
  this.uMvMatrix = null;
  this.uPMatrix = null;
  this.uColor = null;

  this.uMvMatrixDepth = null;
  this.uPMatrixDepth = null;
  this.uColorDepth = null;

  this.shader = null;
  this.depthShader = null;

  this.clearColor = [ 0, 0, 0, 0 ];
  this.boundTexture = this.s.ctx.INVALID_VALUE;
  this.boundTextureArray = this.s.ctx.INVALID_VALUE;
  this.boundProgram = this.s.ctx.INVALID_VALUE;
  this.boundFramebufferPage = null;
  this.boundFramebufferSprite = null;

  this.pages = [],
  this.pageSize = { width: 1024, height: 1024 },

  this.debug = false,

  this.create();
}

SpriteyWebGL.prototype = {
  create: function() {
    var ctx = this.s.ctx;

    ctx.clearColor(0.0, 0.0, 0.0, 0.0);
    ctx.enable(ctx.BLEND);
    ctx.blendFunc(ctx.SRC_ALPHA, ctx.ONE_MINUS_SRC_ALPHA);

    var vertexShaderCode =
    "  attribute vec3 aVertexPosition;" + "\n" +
    "  attribute vec2 aTextureCoord;" + "\n" + "\n" +

    "  uniform mat4 uMVMatrix;" + "\n" +
    "  uniform mat4 uPMatrix;" + "\n" +
    "  uniform vec4 uVColor;" + "\n" + "\n" +

    "  varying vec2 vTextureCoord;" + "\n" +
    "  varying vec4 vColor;" + "\n" + "\n" +

    "  void main(void) {" + "\n" +
    "    gl_Position = uPMatrix * uMVMatrix * vec4(aVertexPosition, 1.0);" + "\n" +
    "    vTextureCoord = aTextureCoord;" + "\n" +
    "    vColor = uVColor;" + "\n" +
    "  }";

    var fragmentShaderCode =
    "  precision lowp float;" + "\n" + "\n" +

    "  varying vec4 vColor;" + "\n" +
    "  varying vec2 vTextureCoord;" + "\n" + "\n" +

    "  uniform sampler2D uSampler;" + "\n" + "\n" +

    "  void main(void) {" + "\n" +
    //"    gl_FragColor = vec4(0, 0, 0, 1);" + "\n" +
    "    gl_FragColor = texture2D(uSampler, vTextureCoord) * vColor;" + "\n" +
    "  }";

    var depthVertexShaderCode =
    "  attribute vec3 aVertexPosition;" + "\n" +
    "  uniform mat4 uMVMatrix;" + "\n" +
    "  uniform mat4 uPMatrix;" + "\n" +
    "  void main(void) {" + "\n" +
    "    gl_Position = uPMatrix * uMVMatrix * vec4(aVertexPosition, 1.0);" + "\n" +
    "  }";

    var depthFragmentShaderCode =
    "  precision lowp float;" + "\n" +
    "  uniform vec4 uColor;" + "\n" +
    "  void main(void) {" + "\n" +
    "    gl_FragColor = uColor;" + "\n" +
    "  }";

    this.shader = this.createShader(vertexShaderCode,
                                    fragmentShaderCode);
    this.depthShader = this.createShader(depthVertexShaderCode,
                                         depthFragmentShaderCode);

    // Setup the depth shader (for render-to-texture-atlas)
    var aVertexDepth =
      ctx.getAttribLocation(this.depthShader, "aVertexPosition");
    this.uMvMatrixDepth = ctx.getUniformLocation(this.depthShader, "uMVMatrix");
    this.uPMatrixDepth = ctx.getUniformLocation(this.depthShader, "uPMatrix");
    this.uColorDepth = ctx.getUniformLocation(this.depthShader, "uColor");

    ctx.enableVertexAttribArray(aVertexDepth);

    // Setup the sprite shader (texture attribute will be setup on-demand)
    var aVertex = ctx.getAttribLocation(this.shader, "aVertexPosition");

    this.uMvMatrix = ctx.getUniformLocation(this.shader, "uMVMatrix");
    this.uPMatrix = ctx.getUniformLocation(this.shader, "uPMatrix");
    this.uColor = ctx.getUniformLocation(this.shader, "uVColor");
    var uSampler = ctx.getUniformLocation(this.shader, "uSampler");

    ctx.enableVertexAttribArray(aVertex);

    this.setShader(this.shader);
    ctx.uniform4fv(this.uColor, [1, 1, 1, 1]);
    ctx.uniform1i(uSampler, 0);

    var vertices = [ 0, 0, -0.5,
                     1, 0, -0.5,
                     1, 1, -0.5,
                     0, 1, -0.5 ];
    var quadVertexBuffer = ctx.createBuffer();
    ctx.bindBuffer(ctx.ARRAY_BUFFER, quadVertexBuffer);
    ctx.bufferData(ctx.ARRAY_BUFFER, new Float32Array(vertices),
                   ctx.STATIC_DRAW);
    ctx.vertexAttribPointer(aVertex, 3, ctx.FLOAT, false, 0, 0);
    ctx.vertexAttribPointer(aVertexDepth, 3, ctx.FLOAT, false, 0, 0);

    this.resize(this.canvas.width, this.canvas.height);
  },

  createShader: function(vertexShaderCode, fragmentShaderCode) {
    var ctx = this.s.ctx;

    var vertexShader = ctx.createShader(ctx.VERTEX_SHADER);
    ctx.shaderSource(vertexShader, vertexShaderCode);
    ctx.compileShader(vertexShader);
    if (!ctx.getShaderParameter(vertexShader, ctx.COMPILE_STATUS)) {
      console.error(ctx.getShaderInfoLog(vertexShader));
      throw "Failed to compile vertex shader";
    }

    var fragmentShader = ctx.createShader(ctx.FRAGMENT_SHADER);
    ctx.shaderSource(fragmentShader, fragmentShaderCode);
    ctx.compileShader(fragmentShader);
    if (!ctx.getShaderParameter(fragmentShader, ctx.COMPILE_STATUS)) {
      console.error(ctx.getShaderInfoLog(fragmentShader));
      throw "Failed to compile fragment shader";
    }

    var shader = ctx.createProgram();
    ctx.attachShader(shader, vertexShader);
    ctx.attachShader(shader, fragmentShader);
    ctx.linkProgram(shader);

    if (!ctx.getProgramParameter(shader, ctx.LINK_STATUS)) {
      throw "Failed to link shader program";
    }

    return shader;
  },

  bindTexture: function(texture) {
    if (this.boundTexture !== texture) {
      this.boundTexture = texture;
      this.s.ctx.bindTexture(this.s.ctx.TEXTURE_2D, texture);
    }
  },

  setShader: function(shader) {
    if (this.boundProgram === shader) {
      return;
    }

    this.s.ctx.useProgram(shader);
    this.boundProgram = shader;
  },

  enableTextures: function() {
    if (!this.aTexture) {
      this.aTexture =
        this.s.ctx.getAttribLocation(this.shader, "aTextureCoord");
      this.s.ctx.enableVertexAttribArray(this.aTexture);
    }
  },

  bindTextureArray: function(array) {
    if (this.boundTextureArray !== array) {
      this.enableTextures();
      this.boundTextureArray = array;

      var ctx = this.s.ctx;
      ctx.bindBuffer(ctx.ARRAY_BUFFER, array);
      ctx.vertexAttribPointer(this.aTexture, 2, ctx.FLOAT, false, 0, 0);
    }
  },

  save: function() {
    if (!this.matrixStack[this.matrixStack.length]) {
      this.matrixStack[this.matrixStack.length] = mat4.clone(this.mvMatrix);
    } else {
      mat4.copy(this.matrixStack[this.matrixStack.length], this.mvMatrix);
    }
    this.matrixStack.length ++;
  },

  restore: function() {
    mat4.copy(this.mvMatrix, this.matrixStack[--this.matrixStack.length]);
  },

  reset: function() {
    mat4.identity(this.mvMatrix);

    if (this.boundFramebufferPage) {
      this.translate(this.boundFramebufferSprite.data.rect.x,
                     this.boundFramebufferSprite.data.rect.y);
    }
  },

  translate: function(x, y) {
    mat4.translate(this.mvMatrix, this.mvMatrix, [x, y, 0]);
  },

  rotate: function(radians) {
    mat4.rotateZ(this.mvMatrix, this.mvMatrix, radians);
  },

  scale: function(width, height) {
    mat4.scale(this.mvMatrix, this.mvMatrix, [width, height, 1]);
  },

  setOpacity: function(opacity) {
    this.setShader(this.shader);
    this.s.ctx.uniform4fv(this.uColor, [1, 1, 1, opacity]);
  },

  clear: function(color) {
    var ctx = this.s.ctx;

    if (this.boundFramebufferPage !== null) {
      var rect = this.boundFramebufferSprite.data.rect;

      this.save();
      this.reset();
      this.scale(rect.width, rect.height);
      this.setShader(this.depthShader);
      ctx.uniform4fv(this.uColorDepth, color);
      ctx.uniformMatrix4fv(this.uMvMatrixDepth, false, this.mvMatrix);
      ctx.disable(ctx.BLEND);
      ctx.drawArrays(ctx.TRIANGLE_FAN, 0, 4);
      ctx.enable(ctx.BLEND);
      this.restore();

      return;
    }

    if (color[0] != this.clearColor[0] ||
        color[1] != this.clearColor[1] ||
        color[2] != this.clearColor[2] ||
        color[3] != this.clearColor[3]) {
      this.clearColor = color;
      ctx.clearColor(color[0], color[1], color[2], color[3]);
    }
    ctx.clear(ctx.COLOR_BUFFER_BIT);
  },

  loadSprite: function(sprite, image) {
    var ctx = this.s.ctx;

    // Find a texture page
    var page = null;
    var oversized = false;
    if (sprite.width <= this.pageSize.width / 2 &&
        sprite.height <= this.pageSize.height / 2) {
      for (var i = 0, iLen = this.pages.length; i < iLen; i++) {
        if (!this.pages[i].full) {
          page = this.pages[i];
          break;
        }
      }
    } else {
      oversized = true;
    }

    // Create a page if there are none available
    if (!page) {
      var width, height;

      if (oversized) {
        width = sprite.width;
        height = sprite.height;
        if (this.debug) {
          console.log('Creating texture page for oversized ' +
                      sprite.width + 'x' + sprite.height + ' sprite');
        }
      } else {
        width = this.pageSize.width;
        height = this.pageSize.height;
        if (this.debug) {
          console.log('Creating new ' + this.pageSize.width + 'x' +
                      this.pageSize.height + ' sprite page');
        }
      }

      page = { full: false, rects: [], 'width': width, 'height': height };
      page.texture = ctx.createTexture();
      if (page.texture === ctx.INVALID_VALUE) {
        throw "Error creating texture page";
      }

      this.bindTexture(page.texture);
      ctx.texParameteri(ctx.TEXTURE_2D, ctx.TEXTURE_MAG_FILTER, ctx.NEAREST);
      ctx.texParameteri(ctx.TEXTURE_2D, ctx.TEXTURE_MIN_FILTER, ctx.NEAREST);
      ctx.texParameteri(ctx.TEXTURE_2D, ctx.TEXTURE_WRAP_S, ctx.CLAMP_TO_EDGE);
      ctx.texParameteri(ctx.TEXTURE_2D, ctx.TEXTURE_WRAP_T, ctx.CLAMP_TO_EDGE);

      ctx.texImage2D(ctx.TEXTURE_2D, 0, ctx.RGBA,
                     page.width, page.height, 0,
                     ctx.RGBA, ctx.UNSIGNED_BYTE, null);

      this.pages.push(page);
    }

    // Find a space to use in the page
    var x, y;
    var space = false;
    if (!page.rects.length) {
      x = 0;
      y = 0;
      space = true;
    } else {
      // FIXME: This is probably not the best packing algorithm
      for (var i = 0, iLen = page.rects.length; i < iLen && !space; i++) {
        var rect = page.rects[i];

        // See if there's space to place the sprite around one of the 8
        // alignments around this rect.
        for (var j = 0; j < 8 && !space; j++) {
          switch(j) {
          case 0:
            // Top, align-left
            x = rect.x;
            y = rect.y - sprite.height;
            break;
          case 1:
            // Top, align-right
            x = (rect.x + rect.width) - sprite.width;
            y = rect.y - sprite.height;
            break;
          case 2:
            // Right, align-top
            x = rect.x + rect.width;
            y = rect.y;
            break;
          case 3:
            // Right, align-bottom
            x = rect.x + rect.width;
            y = (rect.y + rect.height) - sprite.height;
            break;
          case 4:
            // Bottom, align-right
            x = (rect.x + rect.width) - sprite.width;
            y = rect.y + rect.height;
            break;
          case 5:
            // Bottom, align-left
            x = rect.x;
            y = rect.y + rect.height;
            break;
          case 6:
            // Left, align-bottom
            x = rect.x - sprite.width;
            y = (rect.y + rect.height) - sprite.height;
            break;
          case 7:
            // Left, align-top
            x = rect.x - sprite.width;
            y = rect.y;
            break;
          }

          // Check bounds are ok
          if (x < 0 || y < 0 ||
              (x + sprite.width) >= this.pageSize.width ||
              (y + sprite.height) >= this.pageSize.height) {
            continue;
          }

          // Check for collisions with other rects
          var collision = false;
          for (var k = 0; k < iLen; k++) {
            var crect = page.rects[k];
            if (x + sprite.width <= crect.x ||
                y + sprite.height <= crect.y ||
                x >= crect.x + crect.width ||
                y >= crect.y + crect.height) {
              continue;
            }
            collision = true;
            break;
          }

          if (!collision) {
            space = true;
          }
        }
      }

      // If we didn't find space, mark this page as full and create a new one
      // FIXME: May prematurely mark pages as full when encountering large
      //        sprites.
      if (!space) {
        page.full = true;
        return loadSprite(sprite, image);
      }
    }

    // File ourselves on the texture page
    var rect = {'x': x, 'y': y, width: sprite.width, height: sprite.height};
    page.rects.push(rect);
    if (oversized) {
      page.full = true;
    }

    // Upload the texture
    if (this.debug) {
      console.log('Uploading ' + sprite.width + 'x' + sprite.height +
                  ' sprite at ' + x + 'x' + y);
    }
    this.bindTexture(page.texture);
    ctx.texSubImage2D(ctx.TEXTURE_2D, 0, x, y,
                      ctx.RGBA, ctx.UNSIGNED_BYTE, image);

    // Create the texture coordinate array
    var tx1 = x / page.width;
    var ty1 = y / page.height;
    var tx2 = (x + sprite.width) / page.width;
    var ty2 = (y + sprite.height) / page.height;
    var texcoords = [ tx1, ty1,
                      tx2, ty1,
                      tx2, ty2,
                      tx1, ty2 ];

    this.enableTextures();
    var texBuffer = ctx.createBuffer();
    ctx.bindBuffer(ctx.ARRAY_BUFFER, texBuffer);
    ctx.bufferData(ctx.ARRAY_BUFFER, new Float32Array(texcoords),
                   ctx.STATIC_DRAW);
    ctx.vertexAttribPointer(this.aTexture, 2, ctx.FLOAT, false, 0, 0);
    this.boundTextureArray = texBuffer;

    // Store the sprite data
    sprite.data = { 'page': page, 'rect': rect, 'texcoords': texBuffer };
  },

  createBlankSprite: function(sprite, color) {
    var canvas = sprite.data = document.createElement('canvas');
    var ctx = canvas.getContext('2d');
    ctx.fillStyle = "rgba(" + color[0] + ", " + color[1] + "," + color[2] + "," + color[3] + ")";
    ctx.fillRect(-0.5, -0.5, sprite.width + 0.5, sprite.height + 0.5);
    this.loadSprite(sprite, canvas);
  },

  deleteSprite: function(sprite) {
    if (this.boundFramebufferSprite === sprite) {
      this.setRenderTarget(null);
    }

    var page = sprite.data.page;
    var rects = page.rects;
    page.full = false;
    rects.splice(rects.indexOf(sprites.rect), 1);
    sprite.data = null;
  },

  commitSprites: function() {
    for (var i = 0, iLen = this.pages.length; i < iLen; i++) {
      this.pages[i].full = true;
    }
  },

  drawSprite: function(sprite, x, y, xflip, yflip) {
    var ctx = this.s.ctx;

    // Setup model-view matrix
    this.save();
    this.translate(x + sprite.width / 2, y + sprite.height / 2);
    this.scale(xflip ? -1 : 1, yflip ? -1 : 1);
    this.translate(-sprite.width / 2, -sprite.height / 2);
    this.scale(sprite.width, sprite.height);
    this.setShader(this.shader);
    ctx.uniformMatrix4fv(this.uMvMatrix, false, this.mvMatrix);

    // Bind and draw texture
    this.bindTextureArray(sprite.data.texcoords);
    this.bindTexture(sprite.data.page.texture);
    ctx.drawArrays(ctx.TRIANGLE_FAN, 0, 4);

    this.restore();
    return true;
  },

  drawTiles: function(tiles, width, height, tileWidth, tileHeight, x, y) {
    // TODO: A more optimal way of doing this
    var ctx = this.s.ctx;
    this.save();
    this.translate(x, y);
    this.scale(tileWidth, tileHeight);
    this.setShader(this.shader);
    for (var offsetY = 0, i = 0; offsetY < height; offsetY++) {
      for (var offsetX = 0; offsetX < width; offsetX++, i++) {
        ctx.uniformMatrix4fv(this.uMvMatrix, false, this.mvMatrix);
        this.bindTextureArray(tiles[i].data.texcoords);
        this.bindTexture(tiles[i].data.page.texture);
        ctx.drawArrays(ctx.TRIANGLE_FAN, 0, 4);
        this.translate(1, 0);
      }
      this.translate(-width, 1);
    }
    this.restore();
    return true;
  },

  promoteToRenderTarget: function(sprite) {
    var ctx = this.s.ctx;
    var page = sprite.data.page;

    if (page.framebuffer) {
      ctx.bindFramebuffer(ctx.FRAMEBUFFER, page.framebuffer);
      return false;
    }

    // Create framebuffer, depth buffer and projection matrix
    page.framebuffer = ctx.createFramebuffer();
    page.depthBuffer = ctx.createRenderbuffer();
    page.pMatrix = mat4.create();

    // Bind texture to framebuffer
    ctx.bindFramebuffer(ctx.FRAMEBUFFER, page.framebuffer);
    ctx.framebufferTexture2D(ctx.FRAMEBUFFER, ctx.COLOR_ATTACHMENT0,
                             ctx.TEXTURE_2D, page.texture, 0);

    // Bind depth buffer to framebuffer
    ctx.bindRenderbuffer(ctx.RENDERBUFFER, page.depthBuffer);
    ctx.renderbufferStorage(ctx.RENDERBUFFER, ctx.DEPTH_COMPONENT16,
                            page.width, page.height);
    ctx.framebufferRenderbuffer(ctx.FRAMEBUFFER, ctx.DEPTH_ATTACHMENT,
                                ctx.RENDERBUFFER, page.depthBuffer);

    // Initialise projection matrix
    mat4.ortho(page.pMatrix, 0, page.width, 0, page.height, 0, 1);

    return true;
  },

  setRenderTarget: function(sprite) {
    var ctx = this.s.ctx;
    if (!sprite) {
      if (this.boundFramebufferPage === null) {
        return;
      }

      ctx.bindFramebuffer(ctx.FRAMEBUFFER, null);
      this.setShader(this.shader);
      ctx.uniformMatrix4fv(this.uPMatrix, false, this.pMatrix);
      ctx.viewport(0, 0, this.canvas.width, this.canvas.height);
      this.boundFramebufferPage = null;
      this.boundFramebufferSprite = null;

      // Reset the transform and remove depth testing
      this.reset();
      ctx.disable(ctx.DEPTH_TEST);
    } else {
      var page = sprite.data.page;

      if (this.boundFramebufferPage === page &&
          this.boundFramebufferSprite === sprite) {
        return;
      }

      if (this.boundFramebufferPage !== page) {
        this.promoteToRenderTarget(sprite);
        this.setShader(this.shader);
        ctx.uniformMatrix4fv(this.uPMatrix, false, page.pMatrix);
        ctx.viewport(0, 0, page.width, page.height);
      }

      if (this.boundFramebufferPage !== page) {
        this.setShader(this.depthShader);
        ctx.uniformMatrix4fv(this.uPMatrixDepth, false, page.pMatrix);
      }

      this.boundFramebufferPage = page;
      this.boundFramebufferSprite = sprite;
      this.reset();

      if (page.rects.length > 1) {
        // Draw a rect into the depth buffer over the area of the sprite in
        // the texture atlas to use as a clip stencil.
        ctx.enable(ctx.DEPTH_TEST);
        ctx.depthFunc(ctx.ALWAYS);
        ctx.colorMask(false, false, false, false);
        ctx.depthMask(true);
        ctx.clear(ctx.DEPTH_BUFFER_BIT);

        this.save();
        this.setShader(this.depthShader);
        mat4.translate(this.mvMatrix, this.mvMatrix, [0, 0, 0.25]);
        this.scale(sprite.width, sprite.height);
        ctx.uniformMatrix4fv(this.uMvMatrixDepth, false, this.mvMatrix);
        ctx.disable(ctx.BLEND);
        ctx.drawArrays(ctx.TRIANGLE_FAN, 0, 4);
        ctx.enable(ctx.BLEND);
        this.restore();

        ctx.depthFunc(ctx.GEQUAL);
        ctx.colorMask(true, true, true, true);
        ctx.depthMask(false);
      } else {
        ctx.disable(ctx.DEPTH_TEST);
      }
    }
  },

  resize: function(width, height) {
    this.canvas.width = width;
    this.canvas.height = height;
    mat4.ortho(this.pMatrix, 0, width, height, 0, 0, 1);

    if (this.boundFramebufferPage === null) {
      this.s.ctx.viewport(0, 0, width, height);
      this.setShader(this.shader);
      this.s.ctx.uniformMatrix4fv(this.uPMatrix, false, this.pMatrix);
    }
  }
};

