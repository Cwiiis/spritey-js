/*
 * Copyright (c) 2014, 2015 Chris Lord
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

function Spritey(canvas, options) {
  this.default_options = {
    forceCanvas : false,
    canvasOptions : { alpha: true,
                      preserveDrawingBuffer: false }
  };

  this.canvas = canvas;
  this.ctx = null;
  this.renderer = null;
  this.imageLoadingStack = [];

  this.create(options);
}

Spritey.prototype = {
  /**
   * Create the Spritey.js context.
   * @options: A dictionary of options. Valid options:
   *   - forceCanvas: Boolean, whether to bypass WebGL mode.
   *   - canvasOptions: Dictionary used when creating underlying WebGL or
   *                    canvas2d context.
   */
  create: function(options) {
    if (!options) options = {};

    // Check for WebGL availability
    if (!options.forceCanvas && typeof(SpriteyWebGL) !== "undefined") {
      var canvas = document.createElement("canvas");
      var ctx = this._getContext(canvas, "experimental-webgl", options);
      if (!ctx) {
        ctx = this._getContext(canvas, "webgl", options);
      }
      if (!ctx) {
        options.forceCanvas = true;
        console.warn("Unable to create a WebGL Canvas context, falling back to 2D Canvas");
      } else {
        this.ctx = this._getContext(this.canvas, "experimental-webgl", options);
        if (!this.ctx) {
          this.ctx = this._getContext(this.canvas, "webgl", options);
        }

        this.renderer = new SpriteyWebGL(this);
      }
    }

    if (options.forceCanvas || !this.renderer) {
      var canvas = document.createElement("canvas");
      var ctx = this._getContext(canvas, "2d", options);
      if (!ctx) {
        throw "Unable to create a 2D Canvas context";
      } else {
        this.ctx = this._getContext(this.canvas, "2d", options);
        this.renderer = new SpriteyCanvas(this);
      }
    }

    if (!this.ctx || !this.renderer) {
      throw "Failed to initialise renderer";
    }

    this.options = options;
  },

  _getContext: function(canvas, type, options) {
    if (!options) {
      options = this.default_options;
    } else if (!options.canvasOptions) {
      options.canvasOptions = this.default_options.canvasOptions;
    }

    return canvas.getContext(type, options.canvasOptions);
  },

  /**
   * Recreate the underlying canvas context. Useful when handling WebGL
   * context-lost signal.
   */
  refresh: function() {
    this.create(this.options);
  },

  /**
   * Save the current transform state. Pushes the current transform state on
   * a transform stack.
   */
  save: function() {
    this.renderer.save();
  },

  /**
   * Restore the transform state. Pops the last saved transform state from the
   * transform stack.
   */
  restore: function() {
    this.renderer.restore();
  },

  /**
   * Reset all transforms. Resets state to a situation where no transforms are
   * applied.
   */
  reset: function() {
    this.renderer.reset();
  },

  /**
   * Translate the current transform state. Any subsequent drawing commands
   * will be translated by the given values.
   * @x: The X coordinate translation.
   * @y: The Y coordinate translation.
   */
  translate: function(x, y) {
    this.renderer.translate(x, y);
  },

  /**
   * Rotate the current transform state. Any subsequent drawing commands will
   * be rotated around the current origin by the given angle.
   * @radians: The angle of rotation, in radians.
   */
  rotate: function(radians) {
    this.renderer.rotate(radians);
  },

  /**
   * Scale the current transform state. Any subsequent drawing commands will
   * be scaled around the current origin by the given amount.
   * @width: The amount to scale on the X-axis.
   * @height: The amount to scale on the Y-axis.
   */
  scale: function(width, height) {
    this.renderer.scale(width, height);
  },

  /**
   * Sets the opacity of any subsequent drawing commands.
   * @opacity: The opacity, from 0.0 to 1.0.
   */
  setOpacity: function(opacity) {
    this.renderer.setOpacity(opacity);
  },

  /**
   * Clear the current render target to the given colour.
   * @color: An array of RGBA values. RGB values are specified as numbers from
   * 0 to 255, A (alpha) values are specified as a number from 0.0 to 1.0
   */
  clear: function(color) {
    this.renderer.clear(color);
  },

  /**
   * Create a sprite object from an image or canvas tag.
   * @image: An image or canvas tag to use as a source.
   * Returns: A sprite object with three public, read-only properties;
   * 'width' - The image width
   * 'height' - The image height
   * 'loaded' - Boolean, whether the sprite has finished loading. Always true
   * when returning from this function.
   */
  createSprite: function(image) {
    var sprite = {
      "width": image.width,
      "height": image.height,
      "loaded": true
    };
    this.renderer.loadSprite(sprite, image);
    return sprite;
  },

  /**
   * Load an image from a URL. Image loading is managed by a FIFO queue.
   * Images can be resized on load.
   * @url: The image URL
   * @width: [Optional] Width to resize the image to, or -1
   * @height: [Optional] Height to resize the image to, or -1
   * @onload: [Optional] Callback to call when the image has loaded. Callback
   * will contain the loaded and resized image as the only parameter.
   */
  loadImage: function(url, width, height, onload) {
    var imageMeta = {
      "url": url,
      "width": width,
      "height": height,
      "onload": onload,
      "loaded": false,
      "loading": false
    };

    this.imageLoadingStack.push(imageMeta);
    this._loadNextImage();
  },

  _loadNextImage: function() {
    // Remove loaded sprites
    var loading = false;

    for (var i = 0; i < this.imageLoadingStack.length; i++) {
      var imageMeta = this.imageLoadingStack[i];
      if (imageMeta.loaded) {
        this.imageLoadingStack.splice(i, 1);
        delete imageMeta.loading;
        i--;
        continue;
      }

      if (imageMeta.loading) {
        loading = true;
      }
    }

    if (loading || this.imageLoadingStack.length == 0) {
      return;
    }

    function loadImageCallback(self, imageMeta) {
      imageMeta.loading = true;
      return function() {
        imageMeta.loading = false;
        imageMeta.loaded = true;

        // Resize sprite if necessary
        var image = this;
        if ((imageMeta.width && image.width != imageMeta.width) ||
            (imageMeta.height && image.height != imageMeta.height)) {
          image = document.createElement("canvas");
          image.width = imageMeta.width;
          image.height = imageMeta.height;

          var ctx = image.getContext("2d");
          sctx.drawImage(this, 0, 0, this.width, this.height,
                         0, 0, imageMeta.width, imageMeta.height);
        }

        if (imageMeta.onload) {
          imageMeta.onload(image);
        }
        self._loadNextImage();
      };
    }

    var image = new Image();
    var imageMeta = this.imageLoadingStack[0];
    image.onload = loadImageCallback(this, imageMeta);
    image.onerror = function(imageMeta) { return function(e) {
        imageMeta.loading = false;
        imageMeta.loaded = true;
        throw "Error loading " + this.src;
      };
    }(imageMeta);
    image.src = imageMeta.url;
  },

  /**
   * Create a sprite object from a given URL. Images can be resized on load.
   * @url: The URL of the image to load
   * @width: [Optional] Width to resize the image to, or -1
   * @height: [Optional] Height to resize the image to, or -1
   * @onload: [Optional] Callback to call when sprite has finished loading.
   * Will be given the sprite object as the only parameter.
   * Returns: See createSprite()
   */
  loadSprite: function(url, width, height, onload) {
    var sprite = {
      "url": url,
      "width": width,
      "height": height,
      "loaded": false,
      "onload": onload,
    };

    function spriteLoadedCallback(self, sprite) {
      return function(image) {
        sprite.loaded = true;
        sprite.width = image.width;
        sprite.height = image.height;
        self.renderer.loadSprite(sprite, image);
        if (sprite.onload) {
          sprite.onload(sprite, image);
        }
      }
    }
    this.loadImage(url, width, height, spriteLoadedCallback(this, sprite));

    return sprite;
  },

  /**
   * Deprecated, see createBlankSprite().
   */
  loadPlaceholderSprite: function(width, height, color) {
    return this.createBlankSprite(width, height, color);
  },

  /**
   * Create a sprite that is a solid rectangle of a given colour and size.
   * Useful for testing.
   * @width: The width of the sprite
   * @height: The height of the sprite
   * @color: The color of the sprite, as an array of RGBA values.
   * Returns: See createSprite()
   */
  createBlankSprite: function(width, height, color) {
    var sprite = {
      "width": width,
      "height": height,
      "loaded": true
    };

    this.renderer.createBlankSprite(sprite, color);

    return sprite;
  },

  /**
   * Delete a sprite.
   * @sprite: A sprite object
   */
  deleteSprite: function(sprite) {
    this.renderer.deleteSprite(sprite);
  },

  /**
   * Commit sprites. This function is backend specific and shouldn't need to
   * be called, except in exceptional circumstances.
   */
  commitSprites: function() {
    this.renderer.commitSprites();
  },

  /**
   * Draw a sprite at a given position.
   * @sprite: A sprite object
   * @x: The position to draw on the X-axis.
   * @y: The position to draw on the Y-axis.
   * @xflip: [Optional] Whether to flip the sprite on the X-axis
   * @yflip: [Optional] Whether to flip the sprite on the Y-axis
   * Returns: Boolean, true on success.
   */
  drawSprite: function(sprite, x, y, xflip, yflip) {
    if (!sprite || !sprite.loaded) {
      console.error("Tried to draw null or unloaded sprite");
      return false;
    }
    x = Math.round(x);
    y = Math.round(y);

    return this.renderer.drawSprite(sprite, x, y, xflip, yflip);
  },

  /**
   * Draw a tile-array at a given position.
   * @tiles: A 1D array of same-sized sprite objects, in column-major order.
   * @width: The width, in tiles, of the sprite-map
   * @height: The height, in tiles, of the sprite-map
   * @x: The position to draw on the X-axis.
   * @y: The position to draw on the Y-axis.
   * Returns: Boolean, true on success.
   */
  drawTiles: function(tiles, width, height, x, y) {
    if (!tiles) {
      console.error("Tried to draw null tiles");
      return false;
    }
    if (tiles.length < width * height) {
      console.error("Tried to draw " + width + "x" + height +
                    " tiles, but only " + tiles.length + " tiles in array");
      return false;
    }

    // Make sure tiles are loaded and of the same size
    var tileWidth, tileHeight;
    for (var i = 0; i < width * height; i++) {
      if (!tiles[i]) {
        console.error("null tile at array index " + i);
        return false;
      }

      if (!tiles[i].loaded) {
        console.error('Tile not loaded at array index ' + i);
        return false;
      }

      if (!tileWidth) {
        tileWidth = tiles[i].width;
      } else {
        if (tiles[i].width != tileWidth) {
          console.error("Irregular tile size in array");
          return false;
        }
      }

      if (!tileHeight) {
        tileHeight = tiles[i].height;
      } else {
        if (tiles[i].height != tileHeight) {
          console.error("Irregular tile size in array");
          return false;
        }
      }
    }

    // Normalise offset coordinates
    x = Math.round(x || 0);
    y = Math.round(y || 0);

    return this.renderer.
      drawTiles(tiles, width, height, tileWidth, tileHeight, x, y);
  },

  /**
   * Deprecated, see createBlankSprite().
   */
  createRenderTarget: function(width, height) {
    var sprite = this.createBlankSprite(width, height, [0, 0, 0, 0]);
    this.setRenderTarget(sprite);
    return sprite;
  },

  /**
   * Sets the current render target. The current transform matrix will be reset
   * after calling this function.
   * @renderTarget: A sprite object, or null to reset the screen as the
   * render target.
   */
  setRenderTarget: function(renderTarget) {
    this.renderer.setRenderTarget(renderTarget);
  },

  /**
   * Resize the screen render target. Useful if the canvas has changed size.
   * The current contents of the canvas is in an undefined state after calling
   * this function.
   */
  resize: function(width, height) {
    this.renderer.resize(width, height);
  }
};
