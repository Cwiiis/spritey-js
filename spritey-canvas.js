/*
 * Copyright (c) 2014, 2015 Chris Lord
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

function SpriteyCanvas(spritey) {
  this.s = spritey;
  this.canvas = spritey.canvas;
  this.ctx = spritey.ctx;

  this.create();
}

SpriteyCanvas.prototype = {
  create: function() {
  },

  save: function() {
    this.ctx.save();
  },

  restore: function() {
    this.ctx.restore();
  },

  reset: function() {
    this.ctx.setTransform(1, 0, 0, 1, 0, 0);
  },

  translate: function(x, y) {
    this.ctx.translate(x, y);
  },

  rotate: function(radians) {
    this.ctx.rotate(radians);
  },

  scale: function(width, height) {
    this.ctx.scale(width, height);
  },

  setOpacity: function(opacity) {
    this.ctx.globalAlpha = opacity;
  },

  clear: function(color) {
    if (color[0] == 0 && color[1] == 0 && color[2] == 0 && color[3] == 0) {
      this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
    } else {
      this.ctx.save();
      this.ctx.setTransform(1, 0, 0, 1, 0, 0);
      this.ctx.fillStyle = "rgba(" + color[0] + "," + color[1] + "," + color[2] + "," + color[3] + ")";
      this.ctx.fillRect(-0.5, -0.5, this.canvas.width + 0.5, this.canvas.height + 0.5);
      this.ctx.restore();
    }
  },

  loadSprite: function(sprite, image) {
    sprite.data = image;

    // TODO: Atlasing
  },

  createBlankSprite: function(sprite, color) {
    var canvas = sprite.data = document.createElement('canvas');
    var ctx = canvas.getContext('2d');
    canvas.width = sprite.width;
    canvas.height = sprite.height;
    ctx.fillStyle = "rgba(" + color[0] + ", " + color[1] + "," + color[2] + "," + color[3] + ")";
    ctx.fillRect(-0.5, -0.5, sprite.width + 0.5, sprite.height + 0.5);
  },

  deleteSprite: function(sprite) {
    sprite.data = null;
  },

  commitSprites: function() {
  },

  drawSprite: function(sprite, x, y, xflip, yflip) {
    if (!xflip && !yflip) {
      this.ctx.drawImage(sprite.data, x, y);
    } else {
      this.ctx.save();
      this.translate(x + sprite.width/2, y + sprite.height/2);
      this.scale(xflip ? -1 : 1, yflip ? -1 : 1);
      this.ctx.drawImage(sprite.data, -sprite.width/2, -sprite.height/2);
      this.ctx.restore();
    }
    return true;
  },

  drawTiles: function(tiles, width, height, tileWidth, tileHeight, x, y) {
    this.ctx.save();
    this.translate(x, y);
    for (var offsetY = 0, i = 0; offsetY < height; offsetY++) {
      for (var offsetX = 0; offsetX < width; offsetX++, i++) {
        this.ctx.drawImage(tiles[i].data,
                           offsetX * tileWidth, offsetY * tileHeight);
      }
    }
    this.ctx.restore();
    return true;
  },

  setRenderTarget: function(renderTarget) {
    this.reset();

    if (!renderTarget) {
      this.canvas = this.s.canvas;
      this.ctx = this.s.ctx;
      return;
    }

    this.canvas = renderTarget.data;
    this.ctx = this.canvas.getContext("2d");
    if (!this.ctx) {
      throw "Unable to create a 2D Canvas context";
    }
  },

  resize: function(width, height) {
    this.canvas.width = width;
    this.canvas.height = height;
  }
};

